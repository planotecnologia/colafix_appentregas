﻿using AppBlanck.Services;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;


namespace Colafix_AppEntregas
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    [XamlCompilation(XamlCompilationOptions.Compile)]

    public partial class MainPage : ContentPage
    {
        private Entrega entrega;
        public MainPage()
        {
            InitializeComponent();
            //NavigationPage.SetHasNavigationBar(this, true);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            btnCanhoto.IsEnabled = false;
            btnEnvio.IsEnabled = false;

        }


        private async void Button2_Clicked(object sender, EventArgs e)
        {
            //Ask for permission first
            if (string.IsNullOrEmpty(txtPlaca.Text))
            {
                await DisplayAlert("Placa", "Informe a placa do veículo", "Ok");
                return;
            } 

            bool allowed = false;
            var cameraPage = new Page2();
            allowed = await GoogleVisionBarCodeScanner.Methods.AskForRequiredPermission();
            if (allowed)
            {
                cameraPage.CallbackEvent += (object sender2, object e2) => OnReadBarCode(sender2, e2);
                await Navigation.PushModalAsync(new Xamarin.Forms.NavigationPage(cameraPage));
               }
            else await DisplayAlert("Alert", "You have to provide Camera permission", "Ok");
        }


        private async void Button3_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("Ops", ":( Nenhuma câmera detectada.", "OK");
                    return;
                }

                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                if (cameraStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted || storageStatus != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                    cameraStatus = results[Permission.Camera];
                    storageStatus = results[Permission.Storage];
                }

                if (cameraStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted && storageStatus == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {

                    //===HERE IS THE PROBLEM, READ METHOD BUT NOT OPEN CAMERA! NOT ERRORS, NOT EXCEPTION, NOTHING===
                    var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                    {
                        SaveToAlbum = true,
                        //Directory = "Sample",
                        //Name = "test.jpg"
                        CompressionQuality = 75,
                        CustomPhotoSize = 50,
                        PhotoSize = PhotoSize.Small,
                        //MaxWidthHeight = 2000,
                        DefaultCamera = CameraDevice.Rear,
                        Name = "nota.jpg"

                    });


                    if (file == null)
                        return;

                   // await DisplayAlert("File Location", file.Path, "OK");

                    if (MyImage.Source == null)
                    {
                        Console.WriteLine("MyImage.Source == null ==> OK");
                    }

                    MyImage.Source = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        file.Dispose();
                        return stream;
                    });

                    btnEnvio.IsEnabled = true;
                }
                else
                {
                    await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION HERE =: " + ex.Message);
            }

        }


        private async void Button4_Clicked(object sender, EventArgs e)
        {
            GravaEntrega();
        }

        private void OnReadBarCode(object sender, dynamic e)
        {
            try 
            {
                ((ContentPage)sender).Navigation.PopModalAsync();

                string chaveAcesso = e.strBarCode;
                if (chaveAcesso.Length != 44)
                {
                    DisplayAlert("Leitura de nota", "Erro na leitura da nota", "OK");
                    return;
                }
                entrega = new Entrega();
                entrega.CnpjEmitente = chaveAcesso.Substring(6, 14);
                entrega.NumeroNota = chaveAcesso.Substring(25, 9);
                entrega.SerieNota = chaveAcesso.Substring(22, 3);
                entrega.PlacaVeiculo = txtPlaca.Text;

                //          txtChaveAcesso.Text = chaveAcesso;
                txtCNPJ.Text = "CNPJ: " + entrega.CnpjEmitente;
                txtNota.Text = "Nota: " + entrega.NumeroNota;

                // pegar Geolocalizacao

                btnCanhoto.IsEnabled = true;
            }
            catch (Exception ex)
            {
                DisplayAlert("Erro : ", ex.Message, "OK");
            }

        }

        private async void GravaEntrega()
        {
            if (await RestfulService.SetEntrega(entrega))
            {
                await DisplayAlert("Entrega confirmada", "Entrega Confirmada", "OK");
                btnCanhoto.IsEnabled = false;
                btnEnvio.IsEnabled = false;
                txtCNPJ.Text = "CNPJ: ";
                txtNota.Text = "Nota: ";
                MyImage.Source = null;
            }
            else
            {
                await DisplayAlert("Erro no envio dos dados", "Verifique o sinal com internet e tente enviar novamente", "OK");
            }
        }

    }
}


