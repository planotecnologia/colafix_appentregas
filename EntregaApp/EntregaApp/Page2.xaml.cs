﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;

namespace Colafix_AppEntregas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        private dynamic barCode;
        // event callback
        public event EventHandler<object> CallbackEvent;
        protected override void OnDisappearing() => CallbackEvent?.Invoke(this, barCode);
        public Page2()
        {
            InitializeComponent();
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }


        private async void CancelButton_Clicked(object sender, EventArgs e)
        {
            barCode = new System.Dynamic.ExpandoObject();
            barCode.strBarCode = "12345678912345678912356789123456789";
            await Navigation.PopModalAsync();
        }

        private void FlashlightButton_Clicked(object sender, EventArgs e)
        {
            GoogleVisionBarCodeScanner.Methods.ToggleFlashlight();
        }

        private async void CameraView_OnDetected(object sender, GoogleVisionBarCodeScanner.OnDetectedEventArg e)
        {
            List<GoogleVisionBarCodeScanner.BarcodeResult> obj = e.BarcodeResults;

            string result = string.Empty;
            for (int i = 0; i < obj.Count; i++)
            {
                //      result += $"Type : {obj[i].BarcodeType}, Value : {obj[i].DisplayValue}{Environment.NewLine}";
                result += obj[i].DisplayValue;
            }
      
            barCode = new System.Dynamic.ExpandoObject();
            barCode.strBarCode = result;

            await Navigation.PopModalAsync();

 //           Device.BeginInvokeOnMainThread(async () =>
 //           {
 //               await DisplayAlert("Result", result, "OK");
 //               //GoogleVisionBarCodeScanner.Methods.Reset();
 ////               await Navigation.PopModalAsync();

 //           });

        }
    }
}