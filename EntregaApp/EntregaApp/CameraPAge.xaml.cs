﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Colafix_AppEntregas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CameraPAge : ContentPage
    {

        public CameraPAge()
        {
            InitializeComponent();

            takePhoto.Clicked += async (sender, args) =>
            {
                try
                {
                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                    {
                        await DisplayAlert("Ops", ":( Nenhuma câmera detectada.", "OK");
                        return;
                    }

                    var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                    var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
                    {
                        var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
                        cameraStatus = results[Permission.Camera];
                        storageStatus = results[Permission.Storage];
                    }

                    if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
                    {

                        //===HERE IS THE PROBLEM, READ METHOD BUT NOT OPEN CAMERA! NOT ERRORS, NOT EXCEPTION, NOTHING===
                        var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                        {
                            SaveToAlbum = true,
                            //Directory = "Sample",
                            //Name = "test.jpg"
                            CompressionQuality = 75,
                            CustomPhotoSize = 50,
                            PhotoSize = PhotoSize.Medium,
                            MaxWidthHeight = 2000,
                            DefaultCamera = CameraDevice.Rear,
                            Name = "nota.jpg"

                        });


                        if (file == null)
                            return;

                        await DisplayAlert("File Location", file.Path, "OK");

                        if (MyImage.Source == null)
                        {
                            Console.WriteLine("MyImage.Source == null ==> OK");
                        }

                        MyImage.Source = ImageSource.FromStream(() =>
                        {
                            var stream = file.GetStream();
                            file.Dispose();
                            return stream;
                        });
                    }
                    else
                    {
                        await DisplayAlert("Permissions Denied", "Unable to take photos.", "OK");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("EXCEPTION HERE =: " + e);
                }

            };

        }
    }
}

//    private async void Button1_Clicked(object sender, EventArgs e)
//{
//    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
//    {
//        await DisplayAlert("Nenhuma Câmera", ":( Nenuma Câmera disponível.", "OK");
//        return;
//    }

//    //var foto = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
//    //{
//    //    Directory = "Test",
//    //    SaveToAlbum = true,
//    //    CompressionQuality = 75,
//    //    CustomPhotoSize = 50,
//    //    PhotoSize = PhotoSize.MaxWidthHeight,
//    //    MaxWidthHeight = 2000,
//    //    DefaultCamera = CameraDevice.Rear,
//    //    Name = "nota.jpg"
//    //});

//    var foto = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });

//    //var foto = await CrossMedia.Current.TakePhotoAsync(file);

//    if (foto != null)
//    {
//        return;
//    }
//    else
//    {
//        imgFoto.Source = ImageSource.FromStream(() =>
//        {
//            var stream = foto.GetStream();
//            foto.Dispose();
//            return stream;
//        });
//    }

//}
//private async void btnSelecionarImagem_Clicked(object sender, EventArgs e)
//{
//    if (CrossMedia.Current.IsTakePhotoSupported)
//    {
//        var imagem = await CrossMedia.Current.PickPhotoAsync();
//        if (imagem != null)
//        {
//            imgFoto.Source = ImageSource.FromStream(() =>
//            {
//                var stream = imagem.GetStream();
//                imagem.Dispose();
//                return stream;
//            });
//        }
//    }
//}

