﻿using Colafix_AppEntregas;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppBlanck.Services
{
    public static class RestfulService
    {
        private const int Timeout = 10000;

        private static string BaseUrl()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                throw new Exception("Você está sem internet!");
            }

           //  return $"http://192.168.0.52/lojaoServer/appservice.svc/";
            //return $"http://54.233.97.105/lojaoServer/appservice.svc/";
            return $"http://www.appcolafix.com.br:9292/Colafix/appservice.svc/";
        }


        public static async Task<bool> SetEntrega(Entrega entrega)
        {
            try
            {

                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;
                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

                entrega.Latitude = position.Latitude.ToString();
                entrega.Longitude = position.Longitude.ToString();
                

                using (var httpClient = new HttpClient())
                {
                    dynamic entregaApp = new System.Dynamic.ExpandoObject();
                    entregaApp.entrega = entrega;

                    var url = $"{BaseUrl()}setEntrega";
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var x = JsonConvert.SerializeObject(entregaApp);
                    var payload = new StringContent(JsonConvert.SerializeObject(entregaApp), Encoding.UTF8, "application/json");


                    var response = await httpClient.PostAsync(url, payload);

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(response.ReasonPhrase);
                    }
                    return true;

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }



    }
}
