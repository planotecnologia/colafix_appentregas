﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Colafix_AppEntregas

{

    public class Entrega
    {
        public Entrega()
        {

        }
        [JsonProperty("entregaId")]
        public string EntregaId { get; set; }

        [JsonProperty("dataEntrega")]
        public string DataEntrega { get; set; }

        [JsonProperty("numeroNota")]
        public string NumeroNota { get; set; }

        [JsonProperty("serieNota")]
        public string SerieNota { get; set; }

        [JsonProperty("cnpjEmitente")]
        public string CnpjEmitente { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("placaVeiculo")]
        public string PlacaVeiculo { get; set; }

        [JsonProperty("imgComprovante")]
        public string ImgComprovante { get; set; }

        [JsonProperty("dataCurta")]
        public string DataCurta { get; set; }

        [JsonProperty("codCliente")]
        public string CodCliente { get; set; }

        [JsonProperty("nomeCliente")]
        public string NomeCliente { get; set; }
    }

    public class EntregaGeo
    {


        public EntregaGeo()
        {

        }
        public string EntregaId { get; set; }
        public string DataEntrega { get; set; }
        public string NumeroNota { get; set; }
        public string SerieNota { get; set; }
        public string CnpjEmitente { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string PlacaVeiculo { get; set; }
    }


}